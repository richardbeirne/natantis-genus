﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

// Make whatever structs we'll be using, separated into logical blocks
[Serializable]
public struct Race
{
    public int playerRacePosition;
    public Vector3 playerMovement;
    public Vector3 trackDirection;
    public Vector3 playerStartPosition, playerSavedPosition;
    public Quaternion playerStartRotation, playerSavedRotation;
    public float playerShipHealth, bestLapTime;
    public int lapMarker, currentLap, totalLaps, totalRacers;
    public float[] lapTimes;
    public int medalAwarded;
    public bool hasNewHighScore;
    public int spawnedBlockCount, spawnedBlockTotatAllowed;
}

[Serializable]
public struct State
{
    public bool isPaused, playerFinished, userInput, playerDead, playerOutOfBounds, inMenu;
    public float lapTimeBronze;
    public float lapTimeSilver;
    public float lapTimeGold;
}

[Serializable]
public struct Settings
{
    public string filePath;
    public float masterVolume, musicVolume, sfxVolume;
    public bool useAiCamera;
    public string obstacleType;
}

public class Data : MonoBehaviour
{

    //Create a singleton instance to hold all of the global data we'll need
    /*
	 * Single variable because readability. 
	 * I know it's frowned upon, but this stuff is referenced literally everywhere
	 * and reading Data._singletonInstance.thing.someVariable is just horrible.
	 * This is to make it slightly less horrible.
	 */
    public static Data i = null;

    // Setup save directory and file name, an instantiate the structs above
    const string RACE_FILENAME = "race.txt";
    const string SETTINGS_FILENAME = "settings.txt";
    const string STATE_FILENAME = "state.txt";
    public Race race;
    public Settings settings;
    public State state;

    //add any non-persistant vars we might want here
    public bool raceLoaded;
    public GameObject player, startCamera;

    void Awake()
    {
        //Ensure this is the only intance of the singleton
        DontDestroyOnLoad(transform.gameObject);

        if (i == null)
        {
            i = this;
        }
        else if (i != this)
        {
            Destroy(gameObject);
        }

        // Reassign filePath here, just in case
        if (Data.i.settings.filePath == null)
        {
            Data.i.settings.filePath = Application.persistentDataPath;
        }
    }

    void Start()
    {
        //Create structs to hold our data and assign file path
        race = new Race();
        settings = new Settings();
        state = new State();

        Data.i.settings.filePath = Application.persistentDataPath;

        //Load player data, or create some if not exists (pass in true)
        Game_LoadEvent(true);

        //Set game paused state to false, as it's pulled in with the other state data
        //and we don't want the game to start this way.
        Data.i.state.isPaused = false;
    }

    //When a scene is loaded, check if we're currently in a race.
    void OnLevelWasLoaded()
    {
        // if we are, grab the player
        if (SceneManager.GetActiveScene().name == "main")
        {
            player = GameObject.Find("Player");

            //and set the main camera according to settings
            if (settings.useAiCamera)
            {
                startCamera = GameObject.Find("aiCamera");
            }
            else
            {
                startCamera = GameObject.Find("staticCamera");
            }
            startCamera.tag = "MainCamera";
            startCamera.GetComponent<Camera>().enabled = true;
            startCamera.GetComponent<AudioListener>().enabled = true;
        }
    }

    void OnApplicationPause(bool isPaused)
    {
        if (isPaused)
        {
            Game.Events.RacePause();
        }
        else
        {
            Game.Events.RaceResume();
        }
    }

    //Called when the player ship is distroyed
    public void Race_DeathEvent()
    {
        Debug.Log("You lost!");
        Game.Events.RacePause();
    }

    // Called (directly) when the player leaves the track, also by Game_Reset
    public void Race_ResetEvent(bool resetHighScore)
    {
        // Reset all player data
        Game_FirstRun();

        if (resetHighScore)
        {
            Data.i.race.bestLapTime = 0;
        }

        // Reset the player ship's position and rotation to start
        player.transform.position = Data.i.race.playerStartPosition;
        player.transform.rotation = Data.i.race.playerStartRotation;
    }

    // If we exit, quit. Doesn't strictly need its own function, but might have
    // other things in it later.
    void OnApplicationQuit()
    {
        Game.Events.QuitToDesktop();
    }

    //fired when loaded
    public void Game_LoadEvent(bool forceReset)
    {
        // This should really be perameterised, but I might not get around to it.

        // If file, pull data out of it, and make our instance earlier equal it.
        // If not, call a function to set all the variables initially.

        if (File.Exists(Data.i.settings.filePath + "/" + RACE_FILENAME) && !forceReset)
        {
            string jsonRace = File.ReadAllText(Data.i.settings.filePath + "/" + RACE_FILENAME);
            race = JsonUtility.FromJson<Race>(jsonRace);
        }
        else
        {
            Debug.Log("Race file not found, assuming first run.");
            Game_FirstRun();
        }

        if (File.Exists(Data.i.settings.filePath + "/" + SETTINGS_FILENAME) && !forceReset)
        {
            string jsonSettings = File.ReadAllText(Data.i.settings.filePath + "/" + SETTINGS_FILENAME);
            settings = JsonUtility.FromJson<Settings>(jsonSettings);
        }
        else
        {
            Debug.Log("Settings file not found, assuming first run.");
            Game_FirstRun();
        }

        if (File.Exists(Data.i.settings.filePath + "/" + STATE_FILENAME) && !forceReset)
        {
            string jsonState = File.ReadAllText(Data.i.settings.filePath + "/" + STATE_FILENAME);
            state = JsonUtility.FromJson<State>(jsonState);
        }
        else
        {
            Debug.Log("State file not found, assuming first run.");
            Game_FirstRun();
        }
        Debug.Log(Data.i.settings.filePath + " - Load, end");
    }

    // fired when saved (annoyingly, full paths are needed in case accessed from other scripts).
    public void Game_SaveEvent()
    {
        Debug.Log("Saving game data");

        //stringify our data, then write it to text files
        string serializedRace = JsonUtility.ToJson(race);
        string serializedSettings = JsonUtility.ToJson(settings);
        string serializedState = JsonUtility.ToJson(state);

        File.WriteAllText(Data.i.settings.filePath + "/" + RACE_FILENAME, serializedRace);
        File.WriteAllText(Data.i.settings.filePath + "/" + SETTINGS_FILENAME, serializedSettings);
        File.WriteAllText(Data.i.settings.filePath + "/" + STATE_FILENAME, serializedState);
        Debug.Log(Data.i.settings.filePath + " - Save, end");
    }

    //fired only on first run of the game (or if no files exist), to initialise the data.
    void Game_FirstRun()
    {

        //Race struct
        race.playerShipHealth = 1000;
        race.playerRacePosition = 1;
        race.totalRacers = 1;
        race.lapMarker = 0;
        race.currentLap = 0;
        race.totalLaps = 3;
        race.lapTimes = new float[race.totalLaps + 1];
        race.spawnedBlockCount = 0;
        race.spawnedBlockTotatAllowed = 10;

        //Settings struct
        settings.masterVolume = 0;
        settings.musicVolume = 0;
        settings.sfxVolume = 0;
        settings.useAiCamera = false;
        settings.obstacleType = "";

        //State struct
        Data.i.settings.filePath = Application.persistentDataPath;
        state.isPaused = false;
        state.inMenu = true;
        state.playerDead = false;
        state.playerFinished = false;
        state.userInput = false;
        state.lapTimeBronze = 50.0f;
        state.lapTimeSilver = 40.0f;
        state.lapTimeGold = 30.0f;
    }
}
