﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace Game {

	public class Events : MonoBehaviour {

		public static void QuitToDesktop () {
			print ("Quitting application...");
			Application.Quit ();
		}

		public static void QuitToMenu () {
			print ("Loading main menu scene...");
			Data.i.state.isPaused = false;
			RaceResume ();
			SceneManager.LoadScene ("Main_Menu");
		}

		public static void RacePause () {
			Debug.Log ("I'm paused!");
			//Cursor.lockState = CursorLockMode.None;
			Time.timeScale = 0;
			Data.i.state.isPaused = true;
		}

		public static void RaceResume () {
			Debug.Log ("I've resumed!");
			//Cursor.lockState = CursorLockMode.Confined;
			Time.timeScale = 1;
			Data.i.state.isPaused = false;
		}
	}
}
