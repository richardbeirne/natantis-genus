﻿using UnityEngine;
using System.Collections;

public class Loader : MonoBehaviour {

	public Data data;

	// Use this for initialization
	void Awake () {
		
		if (Data.i == null) {
			Instantiate (data);
		}
	}
}
