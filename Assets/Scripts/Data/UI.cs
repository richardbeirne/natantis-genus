﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Game {
	
	public class UI : MonoBehaviour {

		public static void ShowPanel(GameObject panel) {
			panel.SetActive (true);
			panel.GetComponent<CanvasGroup> ().alpha = 1;
		}

		public static void UI_GoToMenu (string menuName) {
			//Grab our next panel, hide our current one, show the next one, set next as current.
			GameObject currentPanel = GameObject.FindGameObjectWithTag("activeUIPanel");
            GameObject nextPanel = GameObject.Find(menuName);

			currentPanel.GetComponent<CanvasGroup> ().alpha = 0;
			nextPanel.GetComponent<CanvasGroup>().alpha = 1;
			nextPanel.GetComponent<RectTransform> ().SetAsLastSibling ();

			currentPanel = nextPanel;
			nextPanel = null;
		}
	}
}