﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	//Grab all of the panels and sliders we're going to need
	public GameObject currentPanel, nextPanel, loadingImage;
	//public Toggle staticCamera, aiCamera;
	//public ToggleGroup cameraToggle;
	public Slider loadingBar;

	//And the audio stuff
	private AudioSource menuItems;
	public AudioClip itemClick;
	public AudioMixer master;

	//And a couple of other things
	private bool firstLoad = true;
	private AsyncOperation async;

	void Start () {
		//Set the starting UI panel to the main menu
		currentPanel = GameObject.Find ("MainPanel");
		UI_GoToMenu (currentPanel.name);
		//This var is here to stop audio playing in GoToMenu on first loading the scene.
		firstLoad = false;

		menuItems = GetComponent<AudioSource> ();
	}

	//Here we load the level asyncronously, and call an IEnum to do it
	public void UI_AsyncClick (string level) {
        UI_GoToMenu("loadingImage");
		StartCoroutine(LoadLevelWithBar(level));
	}

	IEnumerator LoadLevelWithBar (string level) {
		async = SceneManager.LoadSceneAsync (level);
		while (!async.isDone) {
			loadingBar.value = async.progress;
			yield return null;
		}
	}

	//Go to a UI panel as specified by OnClick in the inspector
	public void UI_GoToMenu (string menuName) {
		//If it's not the first time (the user doesn't click the first one)
		if (!firstLoad) {
			PlaySound (itemClick);
		}

		//Grab our next panel, hide our current one, show the next one, set next as current.
		nextPanel = GameObject.Find (menuName);
		currentPanel.GetComponent<CanvasGroup> ().alpha = 0;
		nextPanel.GetComponent<CanvasGroup>().alpha = 1;
		nextPanel.GetComponent<RectTransform> ().SetAsLastSibling ();

		currentPanel = nextPanel;
		nextPanel = null;
	}


    // Gameplay Settings
    public void UI_ObstacleToggle(string typeOfObstacle)
    {
        Data.i.settings.obstacleType = typeOfObstacle;
    }

    public void UI_CameraToggle (bool useAiCam) {
		Data.i.settings.useAiCamera = useAiCam;
	}


	// Audio Settings.

	// Get the slider we want, set the sound level of the mixer, then store that globally
	public void UI_MasterVolume (Slider slider) {
		master.SetFloat ("volumeMaster", slider.value);
		Data.i.settings.masterVolume = slider.value;
	}
	public void UI_MusicVolume (Slider slider) {
		master.SetFloat ("volumeMusic", slider.value);
		Data.i.settings.musicVolume = slider.value;
	}
	public void UI_SFXVolume (Slider slider) {
		master.SetFloat ("volumeSFX", slider.value);
		Data.i.settings.sfxVolume = slider.value;

	}

	void PlaySound (AudioClip clip) {
		menuItems.PlayOneShot (clip);
	}
}
