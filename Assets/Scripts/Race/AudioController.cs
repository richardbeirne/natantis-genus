﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class AudioController : MonoBehaviour {

	//Get an instance of our audioMixer
	public AudioMixer audiomixer;

	// And set the global values as defined in our settings.
	void Awake () {
		audiomixer.SetFloat ("volumeMaster", Data.i.settings.masterVolume);
		audiomixer.SetFloat ("volumeMusic", Data.i.settings.musicVolume);
		audiomixer.SetFloat ("volumeSFX", Data.i.settings.sfxVolume);
	}

    // If we pause the game, apply a high-pass filter to music
    void Update ()
    {
        if (Data.i.state.isPaused)
        {
            audiomixer.SetFloat("musicHighPass", 3500.0f);
        }
        else
        {
            audiomixer.SetFloat("musicHighPass", 0.0f);
        }
    }

}
