﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BlockSpawnController : MonoBehaviour {

	//references to block types.
	public GameObject[] blocks; 
	public GameObject instantiatedBlock;

	//parent gameobject to hold spawners in hierarchy, and array for reference here.
	public GameObject[] spawners;

	//vars for control spawning speed of blocks.
	public float minSpawnRate, maxSpawnRate;
    private float nextSpawn, spawnRate;
	
	// Update is called once per frame
	void Update () {
        if (isAcceptable()) {
			//SpawnBlock ();
		}
	}

    bool isAcceptable() {
        if(Time.time > nextSpawn &&
            Data.i.race.spawnedBlockCount < Data.i.race.spawnedBlockTotatAllowed) {
            return true;
        } else
        {
            return false;
        }
    }
    
    void SpawnBlock() {
        //Pick a random time within a range, and set that to be the next spawn occurance after this one. 
        spawnRate = Random.Range(minSpawnRate, maxSpawnRate);
		nextSpawn = Time.time + spawnRate;        

		//Take a random block type, and spawn it in a random spawner.
		instantiatedBlock = Instantiate (blocks[Random.Range(0, blocks.Length)], 
                            spawners[Random.Range(0, spawners.Length)].transform.position, 
                            Quaternion.identity) as GameObject;
        Data.i.race.spawnedBlockCount++;
	}
}
