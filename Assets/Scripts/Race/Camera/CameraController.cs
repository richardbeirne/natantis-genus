﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject target, cameraLookAt, marker;

	//Rigidbody cameraBody;
	public float maxSpeed = 11.0f, scaleFactor = 0.75f;
	float AiFrameSpeed;
	Vector3 speedVector, targetPosition, goodCameraPosition;
	//bool cameraInPosition = false;

	// Use this for initialization
	void Start () {
		//cameraBody = GetComponent<Rigidbody> ();
	}

	// Update is called once per frame
	void FixedUpdate () {

		//if (target == null) {
		//	targetPosition = Vector3.zero;
		//} else {
		//	targetPosition = target.transform.position;
		//}

		//target.transform.position = marker.transform.position;

		//AiFrameSpeed = maxSpeed * Time.deltaTime;

		//if (cameraInPosition) {
		//	// --- Behavior type selection ---
		//	//speedVector = Seek (transform.position, targetPosition, AiFrameSpeed);
		//	speedVector = Arrive (transform.position, targetPosition);
		//} else {
		//	speedVector = Seek (transform.position, targetPosition, AiFrameSpeed);
		//}

		//cameraBody.AddForce (speedVector);
		transform.LookAt (cameraLookAt.transform.position);
	}

	Vector3 Seek(Vector3 source, Vector3 target, float movingSpeed){
		Vector3 directionToTarget = (target - source);
		directionToTarget.Normalize ();
		Vector3 speedToTarget = directionToTarget *= movingSpeed;
		speedToTarget -= GetComponent<Rigidbody> ().velocity * Time.deltaTime;

		return speedToTarget;
	}

	Vector3 Arrive(Vector3 source, Vector3 destination){
		float distanceToTarget = Vector3.Distance (source, destination);
		Vector3 directionToTarget = (destination - source);
		directionToTarget.Normalize ();

		float speedScaler = distanceToTarget / scaleFactor * Time.deltaTime;
		Vector3 speedToTarget = directionToTarget * speedScaler;
		speedToTarget -= GetComponent<Rigidbody> ().velocity * Time.deltaTime;

		return speedToTarget;
	}

	//void OnTriggerEnter (Collider col) {
	//	if (col.gameObject.name == ("CameraFollow")) {
	//		cameraInPosition = true;
	//	}
	//}
	//void OnTriggerStay (Collider col) {
	//	if (col.gameObject.name == ("CameraFollow")) {
	//		cameraInPosition = true;
	//	}
	//}
	//void OnTriggerExit (Collider col) {
	//	if (col.gameObject.name == ("CameraFollow")) {
	//		cameraInPosition = false;
	//	}
	//}
}
