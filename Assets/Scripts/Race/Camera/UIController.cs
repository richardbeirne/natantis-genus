﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.Collections;
using Game;

public class UIController : MonoBehaviour {
	//main UI objects
	public GameObject GO_positionText, GO_lapText, GO_obstacleText, GO_speedText, GO_healthText, GO_medal;

	//in-game texts
	private Text positionText, lapText, obstacleText, speedText, healthText;

	//UI screen texts/
	private Text pauseFlavourText, finishFlavourText, quitToDesktopButtonText, highScoreText, resumeButtonText, deathText;

	//other things
	private int playerHealth, playerSpeed, medal;
	private bool reallyWantsToQuit = false;
	private float avgLapTime;
	public AudioMixer music;

	// menu objects
	public GameObject GO_DeathMenu, GO_pauseMenu, GO_finishMenu;
	public Button quitToDesktopButton, resumeButton;
	public Text pauseFlavour, finishFlavour, highScore, death;
	public Sprite bronze, silver, gold;
	private Image medalImage;

	// Use this for initialization
	void Start () {
		//Grab all text and image components of gameobjects, just because they're a bit easier to read
		positionText = GO_positionText.GetComponent<Text> ();
		lapText = GO_lapText.GetComponent<Text> ();
		speedText = GO_speedText.GetComponent<Text> ();
		healthText = GO_healthText.GetComponent<Text> ();
        obstacleText = GO_obstacleText.GetComponent<Text> ();
		pauseFlavourText = pauseFlavour.GetComponent<Text> ();
		finishFlavourText = finishFlavour.GetComponent<Text> ();
		highScoreText = highScore.GetComponent<Text> ();
		deathText = death.GetComponent<Text> ();
		quitToDesktopButtonText = quitToDesktopButton.GetComponentInChildren<Text> ();
		resumeButtonText = resumeButton.GetComponentInChildren<Text> ();
		medalImage = GO_medal.GetComponent<Image> ();

        obstacleText.text += Data.i.settings.obstacleType;
	}

	// Update is called once per frame
	void Update () {

		//Grab all of the global inputs we'll need that are floats
		//and convert them to ints to be nicely displayed on the front-end
		playerHealth = Mathf.RoundToInt(Data.i.race.playerShipHealth);
		playerSpeed = Mathf.RoundToInt (Data.i.race.playerMovement.magnitude);

		//Update all the main ui elements here
		positionText.text = "Pos: " + Data.i.race.playerRacePosition + " of " + Data.i.race.totalRacers;
		lapText.text = "Lap: " + Data.i.race.currentLap + " of " + Data.i.race.totalLaps;
		speedText.text = "Speed: " + playerSpeed;
		healthText.text = "Health: " + playerHealth;

		// If we hit escape, trigger the pause menu.
		// If we're already paused, unpause.
		if (Input.GetButtonDown("Cancel") && !Data.i.state.playerFinished) {
			if (!Data.i.state.isPaused) {
				UI_PauseEVent ();
			} else if (Data.i.state.isPaused) {
				UI_ResumeEvent (GO_pauseMenu);
			}
		}

		// If the player finishes their laps
		if (Data.i.state.playerFinished) {
			//call a Race pause, but not a UI pause (we don't want the pause screen showing
			//on top of the Finish screen
			Game.Events.RacePause ();			
			Data.i.state.playerFinished = false;
			UI_FinishEvent ();
		}

		// if the player dies
		if (Data.i.state.playerDead) {
			UI_DeathEvent ();
		}
	}

	// For each event, I'm handling any UI changes locally, and passing off
	// any more global things - data saving, etc. - to the Data singleton.

	// show the death screen, 
	public void UI_DeathEvent () {
		GO_DeathMenu.SetActive (true);
		GO_DeathMenu.GetComponent<CanvasGroup> ().alpha = 1;

		if (Data.i.state.playerOutOfBounds) {
			deathText.text = "YOU FELL OFF!";
		}

		//trigger a system pause
		Game.Events.RacePause ();
	}

	public void UI_FinishEvent () {
		//Now that we've finished, assign the time vars and things we need from the singleton
		avgLapTime = Data.i.race.lapTimes[0];
		medal = Data.i.race.medalAwarded;

		// Show the Finish screen, with appropriate text
		GO_finishMenu.SetActive (true);
		GO_finishMenu.GetComponent<CanvasGroup> ().alpha = 1;
		finishFlavourText.color = Color.white;
		finishFlavourText.text = "Your average time was " + avgLapTime + " seconds per lap!";

		Debug.Log (medalImage.name + " Medal : " + Data.i.race.medalAwarded);

		// And show the correct medal to award (if any)
		switch (medal) {
		case 1:
			medalImage.sprite = gold;
			Debug.Log ("changed gold");
			break;
		case 2:
			medalImage.sprite = silver;
			break;
		case 3: 
			medalImage.sprite = bronze;
			break;
		default:
			print ("No medal");
			break;
		}
		GO_medal.SetActive (true);

		//And if they beat their last high-score, say so
		if (Data.i.race.hasNewHighScore) {
			highScoreText.text = "NEW HIGH SCORE!";
		}
	}

	// Show the pause menu, and set the flavourText
	public void UI_PauseEVent () {
		resumeButtonText.text = "RESUME GAME";

		GO_pauseMenu.SetActive (true);
		GO_pauseMenu.GetComponent<CanvasGroup> ().alpha = 1;
		pauseFlavourText.color = Color.white;
		pauseFlavourText.text = "GAME PAUSED";

		//Trigger a system pause
		Game.Events.RacePause ();
	}

	// Trigger a quit event
	public void UI_QuitToMenuEvent () {
		Game.Events.QuitToMenu ();
	}

	// Double-check with the user, then quit to desktop
	public void UI_QuitToDesktopEvent () {
		if (!reallyWantsToQuit) {
			quitToDesktopButtonText.text = "Really?";
			pauseFlavourText.color = Color.red;
			pauseFlavourText.text = "Are you sure?";
			reallyWantsToQuit = true;
		} else {
			Game.Events.QuitToDesktop ();
		}
	}

	// Reset the player's current race, but NOT the highscore
	public void UI_ResetEVent () {
        Debug.Log("triggered ....0");
		pauseFlavourText.color = Color.red;
		pauseFlavourText.text = "GAME RESET";

		Data.i.Race_ResetEvent (false);

		resumeButtonText.text = "START RACE";
	}

	// hide the pause menu and resume play
	public void UI_ResumeEvent (GameObject uiPanel) {
		pauseFlavourText.text = "";
		uiPanel.GetComponent<CanvasGroup> ().alpha = 0;
		uiPanel.SetActive (false);

		//trigger a system resume
		Game.Events.RaceResume ();
	}

	// Save all the current game data
	public void UI_SaveEVent () {
		pauseFlavourText.color = Color.green;
		pauseFlavourText.text = "GAME SAVED";

		Data.i.Game_SaveEvent ();
	}
}
