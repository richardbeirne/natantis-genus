﻿using UnityEngine;
using System.Collections;

public class LapMarkerController : MonoBehaviour {

	void OnTriggerExit(Collider entity) {
		int lapMarker = Data.i.race.lapMarker;
		int currentLap = Data.i.race.currentLap;

		if (entity.gameObject.tag == "PlayerTracker" && currentLap == lapMarker + 1) {
			Data.i.race.lapMarker++;
		}
	}
}
