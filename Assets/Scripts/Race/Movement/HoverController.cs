﻿using UnityEngine;
using System.Collections;

public class HoverController : MonoBehaviour {

	private Rigidbody playerRigidbody;
	private bool userInput;
	private Quaternion desiredRotation;
	private float enginePower, floorDistance;

	public float thrustForce, maxThrustPower, hoverHeight;

	void Awake () {
		playerRigidbody = GetComponent<Rigidbody> ();
	}

    void Start() {
        Data.i.race.playerStartPosition = transform.position;
        Data.i.race.playerStartRotation = transform.rotation;
    }

	void FixedUpdate () {
		
		RaycastHit hit;
		Ray hoverRay = new Ray (transform.position, Vector3.down);

		// If there's something underneath us
		if (Physics.Raycast (hoverRay, out hit, hoverHeight * 3)) {
			// And if it's closer than the distance we want the ship to hover off the ground
			if (hit.distance < hoverHeight) {
				// Set a thrust force and range
				enginePower = Mathf.Clamp ((thrustForce * thrustForce / hit.distance), 0, maxThrustPower);

				// Apply thrust force to keep ship floating
				playerRigidbody.AddForce (transform.up * (enginePower));
			} 
			// If we go too high, add an extra force on top of gravity to keep us stuck to the track better
			else if (hit.distance > hoverHeight * 2) {
				floorDistance = hit.distance;
				enginePower = Mathf.Clamp((thrustForce * thrustForce / hit.distance), 0, 2000);
				playerRigidbody.AddForce (-transform.up * (enginePower));
			}
			// But always adjust to whatever the angle of the ground is
			desiredRotation = Quaternion.FromToRotation (transform.up, hit.normal);
			desiredRotation = desiredRotation * transform.rotation;
			transform.rotation = Quaternion.Lerp (transform.rotation, desiredRotation, 0.07f);

			//Debug.DrawRay (playerRigidbody.transform.position, new Vector3 (hit.normal.x, hit.normal.y, hit.normal.z));
		}
	}	
}
