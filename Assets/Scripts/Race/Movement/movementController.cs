﻿using UnityEngine;
using System.Collections;

public class MovementController : MonoBehaviour {

	private Rigidbody playerRigidbody;
	private float forwardMovement, turningMovement, clampedSoundValue;
	private bool isStopped = true, isReversing = false;
	public AudioSource engines;

	public float engineThrust, reverseThrust, maneuverability, tiltAmount;

	void Awake () {
		playerRigidbody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {

		// Grab hor and vert input axis
		forwardMovement = Input.GetAxis ("Vertical");
		turningMovement = Input.GetAxis ("Horizontal");

		clampedSoundValue = Mathf.Clamp (forwardMovement, 0, 1.0f);

		engines.pitch = clampedSoundValue;
		engines.volume = clampedSoundValue;

		// Check if we're stopped
		isStopped = (Data.i.race.playerMovement.magnitude == 0) ? true : false;

	}

	void FixedUpdate () {
		//Assign any global things that need assigning
		Data.i.race.playerMovement = playerRigidbody.velocity;

		if (isReversing == true) {
			//Debug.Log ("reversing");
		}

		// Forward and back movement
		if (forwardMovement > 0) {
			EngageEngines ();
		} else if (forwardMovement < 0) {
			if (isStopped) {
				EngageReverse ();
			} else {
				DeployBrakes ();
			}
		}

		// Turning
		if (turningMovement != 0) {
			Turn (isReversing);
		}
	}

	void EngageEngines () {
		isReversing = false;
		playerRigidbody.AddForce (transform.forward * forwardMovement * engineThrust * 10);
	}

	void DeployBrakes () {
		playerRigidbody.AddForce (transform.forward * forwardMovement * reverseThrust * 20);
	}

	void EngageReverse () {
		isReversing = true;
		playerRigidbody.AddForce (transform.forward * forwardMovement * reverseThrust * 10);
	}

	void Turn (bool reversing) {
		Vector3 heading;

		heading = (reversing) ? -transform.up : transform.up;

		if (turningMovement < 0) {
			tiltAmount = 0.0f;
		} else {
			tiltAmount = 0.0f;
		}

		//go whatever way we specified
		playerRigidbody.AddRelativeTorque (heading * maneuverability * turningMovement);
		playerRigidbody.AddRelativeTorque (transform.forward * tiltAmount);
		//transform.localRotation = Quaternion.Lerp
		//transform.Rotate (Vector3.up * turningMovement * 10);
	}
}
