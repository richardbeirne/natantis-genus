﻿using UnityEngine;
using System.Collections;

public class BlockController : MonoBehaviour
{

    private Rigidbody objectRigidbody;
    private Quaternion desiredRotation;
    private float enginePower, floorDistance;
    private float damage = 10;

    public AudioSource damageSource;
    public float thrustForce, maxThrustPower, hoverHeight;

    void Awake()
    {
        objectRigidbody = GetComponent<Rigidbody>();
    }

    //This handles the hovering of the obstacles
    void FixedUpdate()
    {

        RaycastHit hit;
        Ray hoverRay = new Ray(transform.position, Vector3.down);

        // If there's something underneath us
        if (Physics.Raycast(hoverRay, out hit, hoverHeight * 3))
        {
            // And if it's closer than the distance we want the ship to hover off the ground
            if (hit.distance < hoverHeight)
            {
                // Set a thrust force and range
                enginePower = Mathf.Clamp((thrustForce * thrustForce / hit.distance), 0, maxThrustPower);

                // Apply thrust force to keep ship floating
                objectRigidbody.AddForce(Vector3.up * (enginePower));
            }
            // If we go too high, add an extra force on top of gravity to keep us stuck to the track better
            else if (hit.distance > hoverHeight * 2)
            {
                floorDistance = hit.distance;
                enginePower = Mathf.Clamp((thrustForce * thrustForce / hit.distance), 0, 2000);
                objectRigidbody.AddForce(transform.up * (enginePower));
            }
            // But always adjust to whatever the angle of the ground is
            desiredRotation = Quaternion.FromToRotation(transform.up, hit.normal);
            desiredRotation = desiredRotation * transform.rotation;
            transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, 0.07f);

            //Debug.DrawRay (playerRigidbody.transform.position, new Vector3 (hit.normal.x, hit.normal.y, hit.normal.z));
        }
    }

    //This handles collisions and damage
    void OnCollisionEnter(Collision entity)
    {
        //calculate and deal damage
        damage = entity.impulse.magnitude / 10;
        Data.i.race.playerShipHealth -= damage;

        //Now what we've used damage for health, convert it for the noise
        damage /= 20;

        //make a noise at the appropriate volume.
        damageSource.volume = Mathf.Clamp(damage, 0.5f, 1.0f);
        if (!damageSource.isPlaying)
        {
            damageSource.Play();
        }

        //if player ship is destroyed on collision
        if (Data.i.race.playerShipHealth <= 0)
        {
            Data.i.state.playerDead = true;
        }
    }
}
