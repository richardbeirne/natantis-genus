﻿using UnityEngine;
using System.Collections;

public class BoostPadController : MonoBehaviour {

	private float boost = 30.0f;
	private AudioSource boostSound;

	void Start () {
		boostSound = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter (Collider entity) {

		//If the player passes over us, boost the ship.
		if (entity.gameObject.tag == "PlayerTracker") {
			entity.attachedRigidbody.AddForce (transform.forward * 100 * boost, ForceMode.Impulse);
			boostSound.Play ();
		}
	}
}
