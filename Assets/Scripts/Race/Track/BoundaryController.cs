﻿using UnityEngine;
using System.Collections;

public class BoundaryController : MonoBehaviour {

	// If the player manages to fall of the track, put them back on, and reset the race.
	void OnTriggerExit (Collider entity) {
		if (entity.gameObject.tag == "PlayerTracker") {
			Debug.Log ("The player is outside the track bounds");

			Data.i.state.playerDead = true;
		}
	}
}
