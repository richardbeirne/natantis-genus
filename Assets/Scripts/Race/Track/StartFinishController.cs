﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//This thing handles what happens with the collider, but for lack of a better idea
//it's also going to handle the lap times, win/loss, etc.
public class StartFinishController : MonoBehaviour {

	private float tempTimeStore = 0.0f;

	public Text lapTimeText;

	void OnTriggerEnter(Collider entity) {

		// set these, just for readability
		int lapMarker = Data.i.race.lapMarker;
		int currentLap = Data.i.race.currentLap;
		int totalLaps = Data.i.race.totalLaps;
		float bestLap = Data.i.race.bestLapTime;
		float[] lapTimes = Data.i.race.lapTimes;

		float gold = Data.i.state.lapTimeGold;
		float silver = Data.i.state.lapTimeSilver;
		float bronze = Data.i.state.lapTimeBronze;

		// set some conditions. 
		// - First, is it our last lap.
		bool lastLap = (currentLap == totalLaps) ? true : false;
		// - Second, are we completeing laps like we should
		bool isCompletingLap = (currentLap == lapMarker) ? true : false;
		// - Third, are we still alive
		bool isNotDead = (Data.i.race.playerShipHealth > 0) ? true : false;


		// if we cross the finish line and are an alive player who's actually completing laps, then
		if (entity.gameObject.tag == "PlayerTracker" && isNotDead && isCompletingLap) {

			//grab the total time elapsed, and set that as our lap time
			lapTimes [currentLap] = Time.time - tempTimeStore;
			tempTimeStore += lapTimes [currentLap];

			// then inc currentLap
			Data.i.race.currentLap++;

			lapTimeText.text = "Lap " + Data.i.race.currentLap + "!";
			Invoke ("CleanLapText", 1);

			// and if it's also the last lap
			if (lastLap) {
				//victory! Set the first index in laps to 0 as it isn't being used.
				lapTimes [0] = 0;
				//Then for every lap, add that time to it
				for (int i = 1; i < lapTimes.Length; i++) {
					lapTimes [0] += lapTimes [i];
				}
				//And get the average
				lapTimes [0] = lapTimes [0] / totalLaps;

				// and assign a medal to be awarded based on the average time.
				if (lapTimes [0] < gold) {
					Data.i.race.medalAwarded = 1;
				} else if (lapTimes [0] < silver) {
					Data.i.race.medalAwarded = 2;
				} else if (lapTimes [0] < bronze) {
					Data.i.race.medalAwarded = 3;
				} else {
					// No medal. Handled in UIController
				}

				// if they got a new high-score, set and flag it so the UIController knows
				if (lapTimes [0] < bestLap) {
					bestLap = lapTimes [0];
					Data.i.race.hasNewHighScore = true;
				}

				Debug.Log ("Race over! " +lapTimes[0]+" "+lapTimes[1]+" "+lapTimes[2]+" "+lapTimes[3]);
				//Then set global finished state to true so the UI event can be called.
				Data.i.state.playerFinished = true;
			}
		} else {
			// Something else hit that we don't care about.
		}
	}

	void CleanLapText() {
		lapTimeText.text = "";
	}
}
