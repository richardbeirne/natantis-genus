﻿using UnityEngine;
using System.Collections;

public class CollisionController : MonoBehaviour {

	private float damage;
	public AudioSource damageSource;

	void Start () {
	}

	void OnCollisionEnter (Collision entity) {
		//calculate and deal damage
		damage = entity.impulse.magnitude / 10;
		Data.i.race.playerShipHealth -= damage;

		//Now what we've used damage for health, convert it for the noise
		damage /= 20;

		//make a noise at the appropriate volume.
		damageSource.volume = Mathf.Clamp(damage, 0.5f, 1.0f);
		if (!damageSource.isPlaying) {
			damageSource.Play ();
		}

		//if player ship is destroyed on collision
		if (Data.i.race.playerShipHealth <= 0) {
			Data.i.state.playerDead = true;
		}
	}
}
