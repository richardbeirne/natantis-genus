﻿using UnityEngine;
using System.Collections;

public class FireController : MonoBehaviour {

	public GameObject firePoint, missile;
	public float fireRate = 0.5f;

	private bool isPaused;
	private float nextFire;

	// Use this for initialization
	void Update () {
		isPaused = Data.i.state.isPaused;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetButtonDown("Fire1") && !isPaused && Time.time >= nextFire) {
			Instantiate(missile, firePoint.transform.position, firePoint.transform.rotation);
			nextFire = Time.time + fireRate;

		}
	}
}
