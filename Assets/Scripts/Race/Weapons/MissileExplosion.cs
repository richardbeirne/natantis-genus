﻿using UnityEngine;
using System.Collections;

public class MissileExplosion : MonoBehaviour {

	public float timeToLive = 3.0f;

	private Light expLight;
	private float killTime;

	// Use this for initialization
	void Awake () {
		expLight = GetComponent<Light>();
	}

	void Start () {
		killTime = Time.time + timeToLive;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		expLight.intensity = Mathf.Lerp (expLight.intensity, 0, timeToLive);

		if (Time.time >= killTime) {
			Destroy (gameObject);
		}
	}
}
