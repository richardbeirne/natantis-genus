﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour {

	public float projectileSpeed;
	public GameObject explosion;

	private Rigidbody projectileBody;
	public float timeToLive = 3.0f;

	private float killTime;
	private Vector3 shipSpeed;

	// Grab things we need from references above
	void Start () {
		projectileBody = GetComponentInChildren<Rigidbody> ();
		shipSpeed = Data.i.race.playerMovement;
		killTime = Time.time + timeToLive;
	}

	// Propel the projectile forward
	void FixedUpdate () {
		//shipSpeed = Data.i.race.playerMovement;
		projectileBody.AddForce (((transform.forward * projectileSpeed)), ForceMode.Impulse);
		//projectileBody.AddForce (shipSpeed * shipSpeed.magnitude);

		//Destroy the projectile after a few seconds if it hasn't hit anything
		if (Time.time >= killTime) {
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter (Collision col) {
		Instantiate (explosion, transform.position + new Vector3(0, 0,-1), transform.rotation);
		Destroy (gameObject);
	}
}
